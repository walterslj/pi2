<?php
session_start();
// if(!$_SESSION['ehadm']){
//     Header("Location: login.html");
// }
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <!-- <meta http-equiv="refresh" content="0; url=login.html"/> -->
  <title>EDITAR</title>
</head>

<body>

  <?php
  if ($_SESSION['idjogo'] != 3) {
    $id = $_GET['idjogo'];

    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");
    $stmt = $con->prepare("SELECT nomej, descricao, link from jogos where idjogo = ? ");

    $stmt->bindParam(1, $id);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_OBJ);

    $nomej = $row->nomej;
    $desc = $row->descricao;
    $link = $row->link;
    $_SESSION['idjogo'] = $id;

    echo "<form action='editar2.php' method='post' enctype='multipart/form-data'>";

    echo "<span>Nome do jogo</span>";
    echo "<input type='text' name='nomej' value='$nomej' required>";

    echo "<span>link</span>";
    echo "<input type='text' name='link' value='$link' required>";

    echo "<span>Imagem</span>";
    echo "<input type='file' name='img'>";

    echo "<span>Descrição</span>";
    echo "<textarea name='desc' cols='20' rows='5'>$desc</textarea>";

    echo "<button type='submit'>EDITAR</button>";

    echo "</form>";
  } else {
    echo "<h1>FOI</h1>";
    // Header("Location: visualizar.php");
    $_SESSION['idjogo'] = 0;
  }
  ?>

  <a id="a" href="visualizar.php">
    <p>Voltar</p>
  </a>

</body>

</html>