<?php

  if (isset($_POST['cate'])) {
    $cate = $_POST['cate'];    

    class verificaJogo {
        public $idJogo;
        public $nomej;
        public $cate;
        public $descricao;
        public $link;
        public $img;
    }

    $resposta = new verificaJogo ();

    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");

    $stmt = $con->prepare("SELECT idjogo, nomej, cate, descricao, link, img FROM jogos where cate = ? ");

    $stmt->bindParam(1, $cate);
    $stmt->execute();
    $vetor = [];

    $i = 0;
    while ($row = $stmt->fetch(PDO::FETCH_OBJ)){

        $obj = new verificaJogo ();
        $obj->idJogo = $row->idjogo;
        $obj->nomej = $row->nomej;
        $obj->link = $row->link;
        $obj->img = $row->img;
        $obj->descricao = $row->descricao;

        $vetor[$i] = $obj;
    
        $i++;
    }
 
echo json_encode($vetor);
}