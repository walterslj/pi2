<?php

session_start();
if (!$_SESSION['idcadastro']) {
    Header("Location: ../login.html");
}
$nome = ucfirst($_SESSION['nome']);
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Filme.com</title>
</head>

<body>
    <header>
        <nav class="barra">
            <ul>
                <li><a href="casa.php">Home</a></li>
                <li><a href="jogos.php">Jogos</a></li>
                <li><a href="inserir1.php">Adicionar</a></li>

            </ul>
        </nav>
    </header>

    <main class="container">
        <a href="casa.php">
            <h1>Melhor site do mundo</h1>
        </a>
        <h2>Seja bem vindo(a) <?=$nome?></h2>
        <a id="a" href="sairuser.php">
            <p>Sair</p>
        </a>
    </main>

    <footer>
        <p>Todos direitos revervados de BW GAMES®</p>
    </footer>



</body>

</html>