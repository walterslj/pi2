<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Mostrar jogo</title>
</head>

<body>
  <h1> JOGOS </h1>

  <?php
  session_start();
  $id = $_GET['idjogo'];
  $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");

  // $stmt = $con->prepare("SELECT nomej, descricao, link, img from jogos where idjogo = ? ");
  
  $stmt = $con->prepare("SELECT nomej, descricao, link, img, nome, cate FROM jogos 
  inner join cadastro on cadastro.idcadastro = jogos.idcadastro where idjogo = ?");

  $stmt->bindParam(1, $id);
  $stmt->execute();

  $row = $stmt->fetch(PDO::FETCH_OBJ);

  switch ($row->cate) {
    case "ave":
      $cate = "Aventura";
      break;
    case "ac":
      $cate = "Ação";
      break;
    case "mb":
      $cate = "Moba";
      break;
    case "rpg":
      $cate = "RPG";
      break;
    case "crd":
      $cate = "Corrida";
      break;
    case "so":
      $cate = "Sobrevivencia";
      break;
    case "fps":
      $cate = "FPS";
      break;
  }

  echo "<h2><? $row->nomej ?></h2>";
  echo "<p> $row->descricao </p><br>";
  echo "<a href='#'> $row->link </a><br><br>";
  echo "<img src='data:image/png;base64, $row->img ' height='20px' width='20'><br>";
  echo "<span>Categoria: $cate</span><br/>";
  echo "<span>Criado por " . ucfirst($row->nome) . "</span>";

  if (isset($_SESSION['ehadm'])) {
    echo "<a id='a' href='visualizar.php'>
      <p>Voltar</p>
    </a>";
  } else {
    if (isset($_SESSION['idcadastro'])) {
      echo "<a id='a' href='jogos.php'>
      <p>Voltar</p>
    </a>";
    }
  }
  ?>
</body>

</html>