<?php
session_start();
if (!$_SESSION['idcadastro']) {
    Header("Location: ../login.html");
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
</head>

<body>
    <?php

    $nomej = $_POST['nomej'];
    $desc = $_POST['desc'];
    $link = $_POST['link'];
    $cat = $_POST['cat'];
    $h = $_SESSION['idcadastro'];
    $img = base64_encode(file_get_contents($_FILES['img']['tmp_name']));

    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");
    $stmt = $con->prepare("INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values (?,?,?,?,?,?)");

    $stmt->bindParam(1, $nomej);
    $stmt->bindParam(2, $desc);
    $stmt->bindParam(3, $link);
    $stmt->bindParam(4, $img);
    $stmt->bindParam(5, $h);
    $stmt->bindParam(6, $cat);
    $stmt->execute();

    $sql = $con;

    if ($sql) {
        // mostra na tela Cadastro concluido
        echo "<script> alert('Jogo cadastrado ')</script>";
    } else {
        //Se não mostra o erro
        echo "Error: $sql <br> $conn->error";
    }

    if ($_SESSION['ehadm'] == true) {
        Header("Location: visualizar.php");
    } else {
        Header("Location: jogos.php");
    }

    ?>
</body>

</html>