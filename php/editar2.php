<?php
session_start();
if (!$_SESSION['ehadm']) {
    Header("Location: login.html");
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="0; url=visualizar.php" />
</head>

<body>
    <?php

    $nomej = $_POST['nomej'];
    $desc = $_POST['desc'];
    $link = $_POST['link'];
    $img = base64_encode(file_get_contents($_FILES['img']['tmp_name']));
    $id = $_SESSION['idjogo'];

    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");
    $stmt = $con->prepare("UPDATE jogos set nomej=?, descricao=?, link=?, img=? 
            where idjogo=?");

    $stmt->bindParam(1, $nomej);
    $stmt->bindParam(2, $desc);
    $stmt->bindParam(3, $link);
    $stmt->bindParam(4, $img);
    $stmt->bindParam(5, $id);
    $stmt->execute();

    $sql = $con;

    if ($sql) {
        // mostra na tela Cadastro editado
        Header("Location: editar1.php");
        $idjogo = 3;
        $_SESSION['idjogo'] = $idjogo;
    } else {
        //Se não mostra o erro
        echo "Error: $sql <br> $conn->error";
    }
    ?>
</body>

</html>