<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="teste.css">
  <style>
  </style>
  <title>Recuperar senha</title>
</head>

<body>
  <?php

  $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");

  $email = isset($_POST['email']) ? $_POST['email'] : null;

  if (isset($_POST['email'])) {

    $stmt = $con->prepare("SELECT per, idcadastro from cadastro where email=?");

    $stmt->bindParam(1, $email);

    $stmt->execute();

    $dado = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if ($stmt != null) {
      session_start();
      $_SESSION['idcadastro'] = $dado['idcadastro'];
      $_SESSION['per'] = $dado['per'];

      Header("Location: rsenha2.php");
    }
  }

  ?>
  <form method="post">

    <span>Email</span>
    <input type="email" name="email" placeholder="Seu email" required>

    <button type="submit">Enviar</button>

  </form>
  <a id="a" href="../login.html">
    <p>Voltar</p>
  </a>
</body>

</html>