<?php

if (isset($_POST['srr'])) {
    $srr = $_POST['srr'];

    class achajogo {
        public $idJogo;
        public $cate;
        public $descricao;
        public $img;
        public $nomej;
    }
    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");

    $stmt = $con->prepare("SELECT idjogo, nomej, cate, descricao, link, img FROM jogos where nomej like ? ");
    $srr = "%" . $srr . "%";
    $stmt->bindParam(1, $srr);
    $stmt->execute();

    $vetor = [];
    $i = 0;

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

        $obj = new achajogo();

        $obj->idJogo = $row->idjogo;
        $obj->nomej = $row->nomej;
        $obj->link = $row->link;
        $obj->img = $row->img;
        $obj->cate = $row->cate;
        $obj->descricao = $row->descricao;

        $vetor[$i] = $obj;

        $i++;
    }
    echo json_encode($vetor);
}