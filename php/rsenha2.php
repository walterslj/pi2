<?php
session_start();
if (!$_SESSION['idcadastro']) {
    Header("Location: rsenha1.php");
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">

    <style>
    </style>
    <title>Recuperar senha</title>

</head>

<body>
    <?php
    $con = new PDO("mysql:host=localhost:3308;dbname=bd_pi2", "root", "");
    $email = isset($_POST['res']) ? $_POST['res'] : null;

    if (isset($_POST['res'])) {
        $stmt = $con->prepare("SELECT res from cadastro where idcadastro=?");

        $stmt->bindParam(1, $_SESSION['idcadastro']);

        $stmt->execute();

        $dado = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($dado['res'] == $_POST['res']) {
            Header("Location: rsenha3.php");
        }
    }

    switch ($_SESSION['per']) {
        case "cor":
            $per = "Qual sua cor favorita?";
            break;
        case "pai":
            $per = "Qual nome de seu pai?";
            break;
        case "amigo":
            $per = "Qual nome do seu melhor amigo?";
            break;
    }

    echo "<form method='post'>";

    echo "<span>$per </span>";
    echo "<input type='text' name='res' placeholder='resposta' required>";

    echo "<button type='submit'>Enviar</button>";

    echo "</form>";
    ?>

        <a id="a" href="rsenha1.php">
            <p>Voltar</p>
        </a>

</body>

</html>