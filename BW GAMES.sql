use bd_pi2;

create table cadastro (
  idcadastro int not null auto_increment,
  nome varchar(10) not null,
  email varchar(45) not null unique,
  senha varchar(45) not null,
  per varchar(45) not null,
  res varchar(20)  not null,
  ehadm bool not null default false,
  PRIMARY KEY (idcadastro));
      
  insert into cadastro (email, senha, nome, per, res, ehadm)
		values('admin@adm.com','adm1010','admin','sim','mda', true);
  
  insert into cadastro (email, senha, nome, per, res)
	values ('waltin200@gmail.com', 'azul123', 'Waltin', 'cor', 'red'); 
  insert into cadastro (email, senha, nome, per, res)
	values ('bae@gmail.com', 'verde456', 'Bae', 'cor', 'red');
  insert into cadastro (email, senha, nome, per, res)
	values ('junin@gmail.com', 'poop77', 'Junin', 'cor', 'red');   

create table jogos(
idjogo int not null auto_increment,
nomej varchar(100) not null unique,
descricao varchar(240) not	null,
link varchar(201) not null,
img longtext not null,
visivel bool not null default true,
cate varchar(20) not null,
idcadastro int ,
primary key(idjogo),
foreign key(idcadastro) references cadastro(idcadastro));

create table erros(
id int not null auto_increment,
texto longtext not null,
primary key(id));

INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('ewa','lol','lol','',1,'ac');
INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('dsa','asd','ddede','',1,'ac');

INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('ert','ert','ert','',1,'so');
INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('dfgf','fgdf','fgd','',1,'so');

INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('ewa','cs','cs','',1,'fp');
INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('jkl','jkl','jkl','',1,'fp');

INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('uyi','yui','yui','',1,'ave');
INSERT into jogos (nomej, descricao, link, img, idcadastro, cate) values('vcv','vcb','vbcb','',1,'ave');
    
select * from cadastro;
select * from jogos;
select * from erros;

SELECT nomej, descricao, link, img, nome FROM jogos inner join cadastro on cadastro.idcadastro = jogos.idjogo where idjogo = 1 ;

delete from cadastro where nome ='v';

/*drop table cadastro;*/
/*drop table jogos;*/
/*drop table erros;*/
/*truncate jogos;*/
